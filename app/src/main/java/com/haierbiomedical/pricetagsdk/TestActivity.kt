package com.haierbiomedical.pricetagsdk

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.haierbiomedical.pricetag.ActivityResultHelper

class TestActivity : AppCompatActivity() {
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test)
    }

    fun enterPrice(view: View) {
        val hospitalCode = "520111"
        ActivityResultHelper.init(this, MyScanManger.getInstance()).startPriceTag(hospitalCode)
    }
}