package com.haierbiomedical.pricetagsdk;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.device.ScanManager;
import android.device.scanner.configuration.PropertyID;
import android.device.scanner.configuration.Triggering;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Vibrator;
import android.util.Log;

import androidx.annotation.Keep;
import androidx.annotation.NonNull;

import com.haierbiomedical.pricetag.IScan;
import com.haierbiomedical.pricetag.IScanListener;

/**
 * Created by Hulk on 2019/7/24.
 */
@Keep
public class MyScanManger implements IScan {

    private static volatile MyScanManger instance = null;
    private Context context;
    private ScanListener scanListener;

    public static synchronized MyScanManger getInstance() {
        if (instance == null) {
            instance = new MyScanManger();
        }
        return instance;
    }

    private Vibrator mVibrator;
    private ScanManager mScanManager = new ScanManager();
    private SoundPool soundpool = null;
    private int soundid;
    private boolean isScaning = false;
    private String barcodeStr = "";
    private final static String SCAN_ACTION = ScanManager.ACTION_DECODE;//default action
    private volatile int mode = 0;//1是追溯码扫码 2是滑道扫码

    private BroadcastReceiver mScanReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            isScaning = false;
            soundpool.play(soundid, 1, 1, 0, 0, 1);
            mVibrator.vibrate(100);
            byte[] barcode = intent.getByteArrayExtra(ScanManager.DECODE_DATA_TAG);
            int barcodelen = intent.getIntExtra(ScanManager.BARCODE_LENGTH_TAG, 0);
            byte temp = intent.getByteExtra(ScanManager.BARCODE_TYPE_TAG, (byte) 0);
            Log.i("debug", "----codetype--" + temp);
            barcodeStr = new String(barcode, 0, barcodelen);
//            if (mode != 2) {//滑道页面不判断位数
//                if (barcodeStr.length() != 20) {
//                    PopupUtil.toast("追溯码不正确");
//                    return;
//                }
//            }
            Log.e("@#$%^", "in      scanListener=" + scanListener);
            if (null != scanListener) {
                scanListener.getScanStr(barcodeStr);
            }
            if (null != iScanListener) {
                iScanListener.getScanContent(barcodeStr);
            }
        }
    };

    public MyScanManger() {
    }

    public void initScan(Context context) {
        try {
            if (null != this.context) {
                cleanScan();
            }
            mode = 1;
            this.context = context;
            mVibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
            mScanManager.openScanner();

            mScanManager.switchOutputMode(0);
            soundpool = new SoundPool(1, AudioManager.STREAM_NOTIFICATION, 100); // MODE_RINGTONE
            soundid = soundpool.load("/etc/Scan_new.ogg", 1);
            //设置是否连续扫描
            if (mScanManager.getTriggerMode() != Triggering.CONTINUOUS) {
                mScanManager.setTriggerMode(Triggering.CONTINUOUS);
            }

            IntentFilter filter = new IntentFilter();
            int[] idbuf = new int[]{PropertyID.WEDGE_INTENT_ACTION_NAME, PropertyID.WEDGE_INTENT_DATA_STRING_TAG};
            String[] value_buf = mScanManager.getParameterString(idbuf);
            if (value_buf != null && value_buf[0] != null && !value_buf[0].equals("")) {
                filter.addAction(value_buf[0]);
            } else {
                filter.addAction(SCAN_ACTION);
            }
            context.registerReceiver(mScanReceiver, filter);
        } catch (Exception ex) {
        }
    }

    public void initScan_slide(Context context) {
        try {
            if (null != this.context) {
                cleanScan();
            }
            mode = 2;
            this.context = context;
            mVibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
            mScanManager.openScanner();

            mScanManager.switchOutputMode(0);
            soundpool = new SoundPool(1, AudioManager.STREAM_NOTIFICATION, 100); // MODE_RINGTONE
            soundid = soundpool.load("/etc/Scan_new.ogg", 1);
            //设置是否连续扫描
            if (mScanManager.getTriggerMode() != Triggering.CONTINUOUS) {
                mScanManager.setTriggerMode(Triggering.CONTINUOUS);
            }

            IntentFilter filter = new IntentFilter();
            int[] idbuf = new int[]{PropertyID.WEDGE_INTENT_ACTION_NAME, PropertyID.WEDGE_INTENT_DATA_STRING_TAG};
            String[] value_buf = mScanManager.getParameterString(idbuf);
            if (value_buf != null && value_buf[0] != null && !value_buf[0].equals("")) {
                filter.addAction(value_buf[0]);
            } else {
                filter.addAction(SCAN_ACTION);
            }
            context.registerReceiver(mScanReceiver, filter);
        } catch (Exception ex) {
        }
    }


    public void startScan() {
        try {
            if (mScanManager == null) {
                return;
            }
            mScanManager.stopDecode();
            isScaning = true;
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            mScanManager.startDecode();
        } catch (Exception e) {
        }
    }

    public void stopScan() {
        try {
            if (mScanManager != null) {
                mScanManager.stopDecode();
                isScaning = false;
            }
        } catch (Exception e) {
        }
    }

    public void cleanScan() {
        try {
            if (null != mScanReceiver) {
                context.unregisterReceiver(mScanReceiver);
                mScanReceiver = null;
                instance = null;
            }
        } catch (Exception e) {
        }
    }

    public boolean isScaning() {
        return isScaning;
    }

    public void setScanListener(ScanListener scanListener) {
        this.scanListener = scanListener;
    }

    IScanListener iScanListener;

    @Override
    public void setScanListener(@NonNull IScanListener listener) {
        iScanListener = listener;
    }

    public interface ScanListener {
        void getScanStr(String str);
    }
}
