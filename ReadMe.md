# **价签sdk集成文档**

- ## 引入aar(price_tag_1.0.aar)

- ## gradle集成三方库


```gradle
implementation 'com.squareup.retrofit2:retrofit:2.5.0'
implementation 'com.squareup.retrofit2:converter-gson:2.5.0'
implementation 'com.jakewharton.retrofit:retrofit2-rxjava2-adapter:1.0.0'
implementation 'com.trello.rxlifecycle2:rxlifecycle-components:2.1.0'
implementation 'com.trello.rxlifecycle3:rxlifecycle-kotlin:3.1.0'
implementation 'io.reactivex.rxjava3:rxandroid:3.0.0'
implementation 'io.reactivex.rxjava3:rxjava:3.0.0'
implementation 'com.google.code.gson:gson:2.8.9'
```

- ## 实现IScan接口，自定义扫码功能


```Kotlin
interface IScan {
    /**
     * 扫码初始化
     *
     * @param context
     */
    fun initScan(context: Context)

    /**
     * 开始扫描
     */
    fun startScan()

    /**
     * 设置扫码回调
     * @param listener listener
     */
    fun setScanListener(listener: IScanListener)

    /**
     * 结束扫描
     */
    fun stopScan()

    /**
     * 清空扫描
     */
    fun cleanScan()
}
```

- ## 初始化


```kotlin
ActivityResultHelper.init(this, MyScanManger.getInstance()).startPriceTag(${"医院编码"})
```