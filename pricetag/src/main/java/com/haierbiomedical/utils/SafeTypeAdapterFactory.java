package com.haierbiomedical.utils;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;

/**
 * @author 23015341
 */
public class SafeTypeAdapterFactory implements TypeAdapterFactory {

    @Override
    public TypeAdapter create(Gson gson, final TypeToken type) {
        final TypeAdapter delegate = gson.getDelegateAdapter(this, type);
        return new TypeAdapter() {

            @Override
            public void write(JsonWriter out, Object value) throws IOException {
                try {
                    delegate.write(out, value);
                } catch (IOException e) {
                    delegate.write(out, null);
                }
            }

            @Override
            public Object read(JsonReader in) throws IOException {
                try {
                    return delegate.read(in);
                } catch (Exception e) {
                    try {
                        in.skipValue();
                    } catch (Exception ex) {

                    }
                    return null;
                }
            }
        };
    }
}
