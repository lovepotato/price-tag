package com.haierbiomedical.utils

import java.security.MessageDigest
import java.util.TreeMap

object SignUtils {
    const val secret = "4ed1e444b1a2912de5b6f47dc35c6e82"
    fun generatorSign(map: Map<String, String?>?, secret: String?): String {
        //先对map排序
        val sorMap = map?.toSortedMap(comparator)
        //转格式
        val sb = StringBuilder()
        sorMap?.forEach { (k: String?, v: Any?) ->
            if (null != v && "" !== v) {
                sb.append(k).append("=").append(v.toString()).append("hlmvims")
            }
        }
        sb.append("secret").append("=").append(secret)
        //根据map进行加密
        return getHash(sb.toString())
    }

    fun generatorSignJytc(map: Map<String?, Any?>?, secret: String?): String {
        //先对map排序
        val sorMap = sorMap(map)
        //转格式
        val sb = StringBuilder()
        sorMap.forEach { (k: String?, v: Any?) ->
            if (null != v && "" !== v) {
                sb.append(k).append("=").append(v.toString()).append("jytc")
            }
        }
        sb.append("secret").append("=").append(secret)
        //根据map进行加密
        return getHash(sb.toString())
    }


    /**
     * 对参数进行排序
     *
     * @param map
     * @return
     */
    fun sorMap(map: Map<String?, Any?>?): Map<String?, Any?> {
        val hashMap = TreeMap<String?, Any?>(comparator)
        hashMap.putAll(map!!)
        return hashMap
    }

    private fun getHash(str: String): String {
        try {
            val md = MessageDigest.getInstance("SHA-256")
            md.update(str.toByteArray())
            val b = md.digest()
            val sb = StringBuffer()
            for (i in b.indices) {
                var v = b[i].toInt()
                v = if (v < 0) 0x100 + v else v
                val cc = Integer.toHexString(v)
                if (cc.length == 1) {
                    sb.append('0')
                }
                sb.append(cc)
            }
            return sb.toString()
        } catch (e: Exception) {
        }
        return ""
    }

    val comparator = kotlin.Comparator { key1: String, key2: String -> key1.compareTo(key2) }
}