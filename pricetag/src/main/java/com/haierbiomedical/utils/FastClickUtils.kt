package com.haierbiomedical.utils

object FastClickUtils {
    private var mLastClickTime: Long = 0
    val isFastClick: Boolean
        get() = isFastClick(600)

    fun isFastClick(delay: Long): Boolean {
        if (System.currentTimeMillis() - mLastClickTime < delay) {
            return true
        }
        mLastClickTime = System.currentTimeMillis()
        return false
    }
}