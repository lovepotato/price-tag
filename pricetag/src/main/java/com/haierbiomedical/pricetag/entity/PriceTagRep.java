package com.haierbiomedical.pricetag.entity;

import androidx.annotation.Keep;

import java.util.List;

@Keep
public class PriceTagRep {

    /**
     * id : 1
     * tagCode : 123456789
     * tagName : 新冠疫苗
     * tagStatus : 0
     * hospitalId : 6
     * iceboxId : 0
     * iceboxNo :
     * vaccineCode :
     * vaccineName :
     * batchNo :
     * manufacturerCode :
     * manufacturerName :
     * validityDate : null
     * isDelete : 0
     * createTime : [2023,1,16,17,58,47]
     * createId : 53
     * updateTime : [2023,1,16,17,58,47]
     * updateId : 53
     * remark :
     */

    private int id;
    private String tagCode;
    private String tagName;
    private int tagStatus;
    private int hospitalId;
    private int iceboxId;
    private String iceboxNo;
    private String vaccineCode;
    private String vaccineName;
    private String batchNo;
    private String manufacturerCode;
    private String manufacturerName;
    private long validityDate;
    private int isDelete;
    private int createId;
    private int updateId;
    private String remark;
    private String specType;
    private String price;

    public String getSpecType() {
        return specType;
    }

    public String getPrice() {
        return price;
    }

    private List<Integer> createTime;
    private List<Integer> updateTime;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTagCode() {
        return tagCode;
    }

    public void setTagCode(String tagCode) {
        this.tagCode = tagCode;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public int getTagStatus() {
        return tagStatus;
    }

    public void setTagStatus(int tagStatus) {
        this.tagStatus = tagStatus;
    }

    public int getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(int hospitalId) {
        this.hospitalId = hospitalId;
    }

    public int getIceboxId() {
        return iceboxId;
    }

    public void setIceboxId(int iceboxId) {
        this.iceboxId = iceboxId;
    }

    public String getIceboxNo() {
        return iceboxNo;
    }

    public void setIceboxNo(String iceboxNo) {
        this.iceboxNo = iceboxNo;
    }

    public String getVaccineCode() {
        return vaccineCode;
    }

    public void setVaccineCode(String vaccineCode) {
        this.vaccineCode = vaccineCode;
    }

    public String getVaccineName() {
        return vaccineName;
    }

    public void setVaccineName(String vaccineName) {
        this.vaccineName = vaccineName;
    }

    public String getBatchNo() {
        return batchNo;
    }

    public void setBatchNo(String batchNo) {
        this.batchNo = batchNo;
    }

    public String getManufacturerCode() {
        return manufacturerCode;
    }

    public void setManufacturerCode(String manufacturerCode) {
        this.manufacturerCode = manufacturerCode;
    }

    public String getManufacturerName() {
        return manufacturerName;
    }

    public void setManufacturerName(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }

    public long getValidityDate() {
        return validityDate;
    }

    public void setValidityDate(long validityDate) {
        this.validityDate = validityDate;
    }

    public int getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(int isDelete) {
        this.isDelete = isDelete;
    }

    public int getCreateId() {
        return createId;
    }

    public void setCreateId(int createId) {
        this.createId = createId;
    }

    public int getUpdateId() {
        return updateId;
    }

    public void setUpdateId(int updateId) {
        this.updateId = updateId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public List<Integer> getCreateTime() {
        return createTime;
    }

    public void setCreateTime(List<Integer> createTime) {
        this.createTime = createTime;
    }

    public List<Integer> getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(List<Integer> updateTime) {
        this.updateTime = updateTime;
    }
}
