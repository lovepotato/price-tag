package com.haierbiomedical.pricetag.entity;

import androidx.annotation.Keep;

@Keep
public class PriceTagVaccInfoRsp {

    /**
     * isLocal :
     * hospitalId : 0
     * approveNo :
     * barCode :
     * level :
     * vaccineName : 卡介苗
     * vaccineCode : 0101
     * manufacturerCode : 01
     * manufacturerName : 北京生物
     * batchNo : 20250111222
     * productDate : 1641052800000
     * validityDate : 1777996800000
     * storeNum : 5
     * lastUseCount : 0
     * specType : 0.5ml
     * prepnType :
     * dosage :
     * category : 0
     * unitNum : 1
     * boxNum : 0
     * parentCode :
     * pkgAmount : 0
     * planKind :
     * syVaccineResDetails : null
     */

    private String isLocal;
    private int hospitalId;
    private String approveNo;
    private String barCode;
    private String level;
    private String vaccineName;
    private String vaccineCode;
    private String manufacturerCode;
    private String manufacturerName;
    private String batchNo;
    private long productDate;
    private long validityDate;
    private String storeNum;
    private String warningNum;
    private String position;
    private String price;

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPosition() {
        return position;
    }

    public String getWarningNum() {
        return warningNum;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public void setWarningNum(String warningNum) {
        this.warningNum = warningNum;
    }

    private int lastUseCount;
    private String specType;
    private String prepnType;
    private String dosage;
    private int category;
    private int unitNum;
    private int boxNum;
    private String parentCode;
    private int pkgAmount;
    private String planKind;
    private Object syVaccineResDetails;

    public String getIsLocal() {
        return isLocal;
    }

    public void setIsLocal(String isLocal) {
        this.isLocal = isLocal;
    }

    public int getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(int hospitalId) {
        this.hospitalId = hospitalId;
    }

    public String getApproveNo() {
        return approveNo;
    }

    public void setApproveNo(String approveNo) {
        this.approveNo = approveNo;
    }

    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getVaccineName() {
        return vaccineName;
    }

    public void setVaccineName(String vaccineName) {
        this.vaccineName = vaccineName;
    }

    public String getVaccineCode() {
        return vaccineCode;
    }

    public void setVaccineCode(String vaccineCode) {
        this.vaccineCode = vaccineCode;
    }

    public String getManufacturerCode() {
        return manufacturerCode;
    }

    public void setManufacturerCode(String manufacturerCode) {
        this.manufacturerCode = manufacturerCode;
    }

    public String getManufacturerName() {
        return manufacturerName;
    }

    public void setManufacturerName(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }

    public String getBatchNo() {
        return batchNo;
    }

    public void setBatchNo(String batchNo) {
        this.batchNo = batchNo;
    }

    public long getProductDate() {
        return productDate;
    }

    public void setProductDate(long productDate) {
        this.productDate = productDate;
    }

    public long getValidityDate() {
        return validityDate;
    }

    public void setValidityDate(long validityDate) {
        this.validityDate = validityDate;
    }

    public String getStoreNum() {
        return storeNum;
    }

    public void setStoreNum(String storeNum) {
        this.storeNum = storeNum;
    }

    public int getLastUseCount() {
        return lastUseCount;
    }

    public void setLastUseCount(int lastUseCount) {
        this.lastUseCount = lastUseCount;
    }

    public String getSpecType() {
        return specType;
    }

    public void setSpecType(String specType) {
        this.specType = specType;
    }

    public String getPrepnType() {
        return prepnType;
    }

    public void setPrepnType(String prepnType) {
        this.prepnType = prepnType;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public int getUnitNum() {
        return unitNum;
    }

    public void setUnitNum(int unitNum) {
        this.unitNum = unitNum;
    }

    public int getBoxNum() {
        return boxNum;
    }

    public void setBoxNum(int boxNum) {
        this.boxNum = boxNum;
    }

    public String getParentCode() {
        return parentCode;
    }

    public void setParentCode(String parentCode) {
        this.parentCode = parentCode;
    }

    public int getPkgAmount() {
        return pkgAmount;
    }

    public void setPkgAmount(int pkgAmount) {
        this.pkgAmount = pkgAmount;
    }

    public String getPlanKind() {
        return planKind;
    }

    public void setPlanKind(String planKind) {
        this.planKind = planKind;
    }

    public Object getSyVaccineResDetails() {
        return syVaccineResDetails;
    }

    public void setSyVaccineResDetails(Object syVaccineResDetails) {
        this.syVaccineResDetails = syVaccineResDetails;
    }
}
