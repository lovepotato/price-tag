package com.haierbiomedical.pricetag.base

import android.accounts.NetworkErrorException
import android.util.Log
import com.haierbiomedical.pricetag.retrofit.OKHTTP_LOG
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import java.util.concurrent.TimeoutException

abstract class BaseObserver<T> : Observer<BaseResponse<T>> {
    override fun onError(e: Throwable) {
        if (e is ConnectException ||
            e is TimeoutException ||
            e is NetworkErrorException ||
            e is UnknownHostException ||
            e is SocketTimeoutException
        ) {
            kotlin.runCatching {
                onFailure(e, false)
                Log.d(OKHTTP_LOG, "okhttp network " + e.message)
            }
        } else {
            kotlin.runCatching {
                Log.d(OKHTTP_LOG, "okhttp error " + e.message)
                onFailure(e, true)
            }
        }
    }

    override fun onComplete() {}
    override fun onSubscribe(d: Disposable) {}
    override fun onNext(tBaseResponse: BaseResponse<T>) {
        if (tBaseResponse.isSuccess()) {
            onSuccess(tBaseResponse)
        } else {
            try {
                onCodeError(tBaseResponse)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    abstract fun onSuccess(tResponse: BaseResponse<T>?)
    abstract fun onCodeError(tResponse: BaseResponse<T>?)

    @Throws(Exception::class)
    abstract fun onFailure(e: Throwable?, netWork: Boolean)
}