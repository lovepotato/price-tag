package com.haierbiomedical.pricetag.base

import androidx.annotation.Keep

/**
 * 统一响应
 *
 * @param <T>
</T> */
@Keep
class BaseResponse<T> {
    var Code: String? = ""
    var Desc: String? = ""
    var Data: T? = null
    var Type: Int = 0
    fun isSuccess(): Boolean {
        return "0000" == Code
    }
}
