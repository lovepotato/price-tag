package com.haierbiomedical.pricetag

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.Keep
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.Group
import androidx.core.content.ContextCompat
import com.haierbiomedical.pricetag.base.BaseObserver
import com.haierbiomedical.pricetag.base.BaseResponse
import com.haierbiomedical.pricetag.dialog.PriceTagCommonTipDialog
import com.haierbiomedical.pricetag.entity.PriceTagRep
import com.haierbiomedical.pricetag.entity.PriceTagVaccInfoRsp
import com.haierbiomedical.pricetag.retrofit.RetrofitUtils
import com.haierbiomedical.pricetag.widget.SpanUtils
import com.haierbiomedical.pricetag.widget.ToastUtils
import com.haierbiomedical.utils.FastClickUtils
import com.trello.rxlifecycle3.kotlin.bindToLifecycle
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

/**
 * 价签绑定页面
 * @author liyang
 */
@Keep
class PriceTagBindActivity : AppCompatActivity(), IScanListener, View.OnClickListener {

    private var iScan: IScan? = null
    private lateinit var blScanPriceTag: Button
    private lateinit var blScanVacc: Button
    private lateinit var blPriceTagCode: TextView
    private lateinit var blVaccCode: TextView
    private lateinit var btnBindPrice: Button
    private lateinit var blPriceUnbind: Button

    private lateinit var tvPriceTagName: TextView
    private lateinit var tvPriceTagCode: TextView

    private lateinit var tvVaccName: TextView
    private lateinit var tvVaccCompany: TextView
    private lateinit var tvVaccBatchNo: TextView
    private lateinit var tvVaccSpecifications: TextView
    private lateinit var tvVaccPrice: TextView
    private lateinit var tvVaccValidity: TextView
    private lateinit var gpVacc: Group

    private var isScannedPriceTag: Boolean = false
    private var hospitalCode: String? = ""
    private var vaccInfoRsp: PriceTagVaccInfoRsp? = null
    private var sdf = SimpleDateFormat("yyyy-MM-dd", Locale.CHINA)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_price_tag_bind)
        hospitalCode = intent.getStringExtra("hospitalCode")
        if (hospitalCode.isNullOrEmpty()) throw RuntimeException("hospitalCode must not null")
        iScan = ActivityResultHelper.getInstance().getmIscan()
        iScan?.setScanListener(this)
        iScan?.initScan(this)

        blScanPriceTag = findViewById(R.id.blScanPriceTag)
        blScanVacc = findViewById(R.id.blScanVacc)
        blPriceTagCode = findViewById(R.id.blPriceTagCode)
        blVaccCode = findViewById(R.id.blVaccCode)
        btnBindPrice = findViewById(R.id.btnBindPrice)
        blPriceUnbind = findViewById(R.id.blPriceUnbind)

        tvPriceTagName = findViewById(R.id.tvPriceTagName)
        tvPriceTagCode = findViewById(R.id.tvPriceTagCode)

        tvVaccName = findViewById(R.id.tvVaccName)
        tvVaccCompany = findViewById(R.id.tvVaccCompany)
        tvVaccBatchNo = findViewById(R.id.tvVaccBatchNo)
        tvVaccSpecifications = findViewById(R.id.tvVaccSpecifications)
        tvVaccPrice = findViewById(R.id.tvVaccPrice)
        tvVaccValidity = findViewById(R.id.tvVaccValidity)
        gpVacc = findViewById(R.id.gpVacc)

        findViewById<ImageView>(R.id.ivBack).setOnClickListener(this)
        blScanPriceTag.setOnClickListener(this)
        blScanVacc.setOnClickListener(this)
        btnBindPrice.setOnClickListener(this)
        blPriceUnbind.setOnClickListener(this)
    }

    private var isPause = false
    override fun onResume() {
        super.onResume()
        isPause = false
        iScan?.startScan()
    }

    override fun onPause() {
        super.onPause()
        isPause = true
        iScan?.stopScan()
    }

    override fun onDestroy() {
        super.onDestroy()
        iScan?.cleanScan()
    }

    override fun getScanContent(string: String) {
        if (isPause) return
        if (FastClickUtils.isFastClick) return
        if (!isScannedPriceTag) {
            blPriceTagCode.text = string
            queryPriceTag(string)
        } else {
            //如果绑定了疫苗信息再扫描疫苗
            if (blPriceUnbind.visibility == View.GONE) {
                blVaccCode.text = string
                queryVaccInfo(string, false)
            } else {
                queryVaccInfo(string, true)
            }
        }
        window.decorView.postDelayed({ iScan?.stopScan() }, 100)
    }

    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.ivBack -> {
                if (blPriceTagCode.text.isNotEmpty()) {
                    val priceTagCommonTipDialog = PriceTagCommonTipDialog.Builder(this)
                        .setHintMessage("绑定价签信息未保存，是否直接返回？").setCancelText("取消")
                        .setConfirmText("确定")
                        .setOnConfirmListener(object : PriceTagCommonTipDialog.OnConfirmListener {
                            override fun onClick() {
                                finish()
                            }
                        }).create()
                    priceTagCommonTipDialog.show()
                } else {
                    finish()
                }
            }

            R.id.blScanPriceTag -> {
                reset()
                iScan?.stopScan()
                iScan?.startScan()
            }

            R.id.blScanVacc -> {
                if (!isScannedPriceTag) {
                    ToastUtils.toast(this, getString(R.string.str_scan_price_tag))
                    return
                }
                iScan?.stopScan()
                iScan?.startScan()
            }

            R.id.btnBindPrice -> {
                bindPrice()
            }

            R.id.blPriceUnbind -> {
                val priceTagCommonTipDialog =
                    PriceTagCommonTipDialog.Builder(this).setHintMessage("确认解绑该疫苗吗？")
                        .setCancelText("取消").setConfirmText("确定")
                        .setOnConfirmListener(object : PriceTagCommonTipDialog.OnConfirmListener {
                            override fun onClick() {
                                unBindPrice()
                            }
                        }).create()
                priceTagCommonTipDialog.show()
            }
        }
    }

    private fun reset() {
        blPriceTagCode.text = ""
        tvPriceTagName.text = "价签名称：- -"
        tvPriceTagCode.text = "价签编码：- -"

        blVaccCode.text = ""
        tvVaccName.text = "疫苗名称：- -"
        tvVaccCompany.text = "生产企业：- -"
        tvVaccBatchNo.text = "疫苗批号：- -"
        tvVaccSpecifications.text = "疫苗品规：- -"
        tvVaccPrice.text = "疫苗价格：- -"
        tvVaccValidity.text = "疫苗效期：- -"

        vaccInfoRsp = null
        isScannedPriceTag = false
        btnBindPrice.isEnabled = false
        blPriceUnbind.visibility = View.GONE
        gpVacc.visibility = View.VISIBLE
    }

    private fun queryPriceTag(string: String) {
        val map = linkedMapOf(
            "hospitalCode" to hospitalCode, "tagCode" to string
        )
        RetrofitUtils.apiUrl?.getTagBindInfo(map)?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())?.bindToLifecycle(tvPriceTagCode)
            ?.subscribe(object : BaseObserver<PriceTagRep>() {
                override fun onSuccess(tResponse: BaseResponse<PriceTagRep>?) {
                    val data = tResponse?.Data
                    isScannedPriceTag = true
                    SpanUtils.with(tvPriceTagName).append("价签名称：")
                        .append(formatStrValue(data?.tagName)).setForegroundColor(
                            ContextCompat.getColor(
                                this@PriceTagBindActivity, R.color.commonTextColor
                            )
                        ).create()
                    SpanUtils.with(tvPriceTagCode).append("价签编码：")
                        .append(formatStrValue(data?.tagCode)).setForegroundColor(
                            ContextCompat.getColor(
                                this@PriceTagBindActivity, R.color.commonTextColor
                            )
                        ).create()
                    if (data?.vaccineCode.isNullOrEmpty()) {
                        gpVacc.visibility = View.VISIBLE
                        blPriceUnbind.visibility = View.GONE
                    } else {
                        gpVacc.visibility = View.GONE
                        blPriceUnbind.visibility = View.VISIBLE

                        SpanUtils.with(tvVaccName).append("疫苗名称：")
                            .append(formatStrValue(data?.vaccineName)).setForegroundColor(
                                ContextCompat.getColor(
                                    this@PriceTagBindActivity, R.color.commonTextColor
                                )
                            ).create()
                        SpanUtils.with(tvVaccCompany).append("生产企业：")
                            .append(formatStrValue(data?.manufacturerName)).setForegroundColor(
                                ContextCompat.getColor(
                                    this@PriceTagBindActivity, R.color.commonTextColor
                                )
                            ).create()
                        SpanUtils.with(tvVaccBatchNo).append("疫苗批号：")
                            .append(formatStrValue(data?.batchNo)).setForegroundColor(
                                ContextCompat.getColor(
                                    this@PriceTagBindActivity, R.color.commonTextColor
                                )
                            ).create()
                        SpanUtils.with(tvVaccSpecifications).append("疫苗品规：")
                            .append(formatStrValue(tResponse?.Data?.specType)).setForegroundColor(
                                ContextCompat.getColor(
                                    this@PriceTagBindActivity, R.color.commonTextColor
                                )
                            ).create()
                        SpanUtils.with(tvVaccPrice).append("疫苗价格：")
                            .append(formatStrValue(tResponse?.Data?.price)).setForegroundColor(
                                ContextCompat.getColor(
                                    this@PriceTagBindActivity, R.color.commonTextColor
                                )
                            ).create()
                        SpanUtils.with(tvVaccValidity).append("疫苗效期：")
                            .append(formatStrValue(sdf.format(Date(tResponse?.Data?.validityDate!!))))
                            .setForegroundColor(
                                ContextCompat.getColor(
                                    this@PriceTagBindActivity, R.color.commonTextColor
                                )
                            ).create()
                    }
                }

                override fun onCodeError(tResponse: BaseResponse<PriceTagRep>?) {
                    blPriceTagCode.text = ""
                    ToastUtils.toast(this@PriceTagBindActivity, tResponse?.Desc)
                }

                override fun onFailure(e: Throwable?, netWork: Boolean) {
                    blPriceTagCode.text = ""
                    ToastUtils.toast(
                        this@PriceTagBindActivity, getString(R.string.str_network_error)
                    )
                }
            })
    }

    private fun formatStrValue(value: String?): String {
        return if (value.isNullOrEmpty()) {
            "--"
        } else {
            value
        }
    }

    private var batchNo: String? = null
    private fun queryVaccInfo(string: String, isChange: Boolean) {
        val map = mutableMapOf(
            "hospitalCode" to hospitalCode, "barCode" to string
        )
        RetrofitUtils.apiUrl?.getCodeInfo(map)?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())?.bindToLifecycle(tvPriceTagCode)
            ?.subscribe(object : BaseObserver<PriceTagVaccInfoRsp>() {
                override fun onSuccess(tResponse: BaseResponse<PriceTagVaccInfoRsp>?) {
                    vaccInfoRsp = tResponse?.Data
                    if (!isChange) {
                        btnBindPrice.isEnabled = true
                        showVaccInfo(vaccInfoRsp)
                    } else {
                        if (batchNo != null && vaccInfoRsp?.batchNo != null && batchNo != vaccInfoRsp?.batchNo) {
                            val priceTagCommonTipDialog =
                                PriceTagCommonTipDialog.Builder(this@PriceTagBindActivity)
                                    .setHintMessage("当前价签已绑定疫苗，是否换绑${vaccInfoRsp?.vaccineName}")
                                    .setCancelText("取消").setConfirmText("确定")
                                    .setOnConfirmListener(object :
                                        PriceTagCommonTipDialog.OnConfirmListener {
                                        override fun onClick() {
                                            showVaccInfo(vaccInfoRsp)
                                            bindPrice()
                                        }
                                    }).create()
                            priceTagCommonTipDialog.show()
                        }
                        batchNo = vaccInfoRsp?.batchNo
                    }
                }

                override fun onCodeError(tResponse: BaseResponse<PriceTagVaccInfoRsp>?) {
                    blVaccCode.text = ""
                    ToastUtils.toast(this@PriceTagBindActivity, tResponse?.Desc)
                }

                override fun onFailure(e: Throwable?, netWork: Boolean) {
                    blVaccCode.text = ""
                    ToastUtils.toast(
                        this@PriceTagBindActivity, getString(R.string.str_network_error)
                    )
                }
            })
    }

    private fun showVaccInfo(data: PriceTagVaccInfoRsp?) {
        SpanUtils.with(tvVaccName).append("疫苗名称：")
            .append(formatStrValue(data?.vaccineName))
            .setForegroundColor(
                ContextCompat.getColor(
                    this@PriceTagBindActivity, R.color.commonTextColor
                )
            ).create()
        SpanUtils.with(tvVaccCompany).append("生产企业：")
            .append(formatStrValue(data?.manufacturerName))
            .setForegroundColor(
                ContextCompat.getColor(
                    this@PriceTagBindActivity, R.color.commonTextColor
                )
            ).create()
        SpanUtils.with(tvVaccBatchNo).append("疫苗批号：")
            .append(formatStrValue(data?.batchNo)).setForegroundColor(
                ContextCompat.getColor(
                    this@PriceTagBindActivity, R.color.commonTextColor
                )
            ).create()
        SpanUtils.with(tvVaccSpecifications).append("疫苗品规：")
            .append(formatStrValue(data?.specType)).setForegroundColor(
                ContextCompat.getColor(
                    this@PriceTagBindActivity, R.color.commonTextColor
                )
            ).create()
        SpanUtils.with(tvVaccPrice).append("疫苗价格：")
            .append(formatStrValue(data?.price)).setForegroundColor(
                ContextCompat.getColor(
                    this@PriceTagBindActivity, R.color.commonTextColor
                )
            ).create()
        SpanUtils.with(tvVaccValidity).append("疫苗效期：")
            .append(formatStrValue(sdf.format(Date(data?.validityDate!!))))
            .setForegroundColor(
                ContextCompat.getColor(
                    this@PriceTagBindActivity, R.color.commonTextColor
                )
            ).create()
    }

    private fun bindPrice() {
        if (vaccInfoRsp == null) return
        val map = mutableMapOf(
            "tagCode" to blPriceTagCode.text.toString(),
            "hospitalCode" to hospitalCode.toString(),
            "vaccineName" to vaccInfoRsp?.vaccineName.toString(),
            "vaccineCode" to vaccInfoRsp?.vaccineCode.toString(),
            "manufacturerCode" to vaccInfoRsp?.manufacturerCode,
            "manufacturerName" to vaccInfoRsp?.manufacturerName,
            "manufacturerName" to vaccInfoRsp?.manufacturerName,
            "validityDate" to sdf.format(vaccInfoRsp?.validityDate),
            "batchNo" to vaccInfoRsp?.batchNo.toString(),
            "storeNum" to vaccInfoRsp?.storeNum,
            "warningNum" to vaccInfoRsp?.warningNum,
            "position" to vaccInfoRsp?.position,
            "operateType" to "21",
        )
        RetrofitUtils.apiUrl?.tagBindOrUpdate(map)?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())?.bindToLifecycle(tvPriceTagCode)
            ?.subscribe(object : BaseObserver<String>() {
                override fun onSuccess(tResponse: BaseResponse<String>?) {
                    btnBindPrice.isEnabled = false
                    reset()
                    iScan?.stopScan()
                    iScan?.startScan()
                    ToastUtils.toastSuccess(this@PriceTagBindActivity, tResponse?.Desc)
                }

                override fun onCodeError(tResponse: BaseResponse<String>?) {
                    ToastUtils.toast(this@PriceTagBindActivity, tResponse?.Desc)
                }

                override fun onFailure(e: Throwable?, netWork: Boolean) {
                    ToastUtils.toast(
                        this@PriceTagBindActivity, getString(R.string.str_network_error)
                    )
                }
            })
    }

    private fun unBindPrice() {
        val map = mutableMapOf(
            "tagCode" to blPriceTagCode.text.toString(),
            "hospitalCode" to hospitalCode,
            "vaccineCode" to vaccInfoRsp?.vaccineCode,
            "manufacturerCode" to vaccInfoRsp?.manufacturerCode,
            "batchNo" to vaccInfoRsp?.batchNo,
            "operateType" to "22",
        )
        RetrofitUtils.apiUrl?.unbindTag(map)?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())?.bindToLifecycle(tvPriceTagCode)
            ?.subscribe(object : BaseObserver<String>() {
                override fun onSuccess(tResponse: BaseResponse<String>?) {
                    btnBindPrice.isEnabled = false
                    gpVacc.visibility = View.VISIBLE
                    blPriceUnbind.visibility = View.GONE
                    blVaccCode.text = ""
                    tvVaccName.text = "疫苗名称：- -"
                    tvVaccCompany.text = "生产企业：- -"
                    tvVaccBatchNo.text = "疫苗批号：- -"
                    tvVaccSpecifications.text = "疫苗品规：- -"
                    tvVaccPrice.text = "疫苗价格：- -"
                    tvVaccValidity.text = "疫苗效期：- -"
                    ToastUtils.toastSuccess(this@PriceTagBindActivity, tResponse?.Desc)

                }

                override fun onCodeError(tResponse: BaseResponse<String>?) {
                    ToastUtils.toast(this@PriceTagBindActivity, tResponse?.Desc)
                }

                override fun onFailure(e: Throwable?, netWork: Boolean) {
                    ToastUtils.toast(
                        this@PriceTagBindActivity, getString(R.string.str_network_error)
                    )
                }
            })
    }

    override fun onBackPressed() {
        if (blPriceTagCode.text.isNotEmpty()) {
            val priceTagCommonTipDialog =
                PriceTagCommonTipDialog.Builder(this)
                    .setHintMessage("绑定价签信息未保存，是否直接返回？")
                    .setCancelText("取消").setConfirmText("确定")
                    .setOnConfirmListener(object : PriceTagCommonTipDialog.OnConfirmListener {
                        override fun onClick() {
                            finish()
                        }
                    }).create()
            priceTagCommonTipDialog.show()
        } else {
            super.onBackPressed()
        }
    }
}