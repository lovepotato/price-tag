package com.haierbiomedical.pricetag.retrofit

import android.util.Log
import okhttp3.Interceptor
import okhttp3.Response
import okhttp3.ResponseBody
import java.io.IOException
import java.util.Locale

/**
 * HTTP Log拦截器代码
 *
 * @author liyang
 */
class LogInterceptor : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        Log.d(OKHTTP_LOG, "request:" + request.url())
        val t1 = System.nanoTime()
        val response = chain.proceed(chain.request())
        val t2 = System.nanoTime()
        Log.i(
            OKHTTP_LOG, String.format(
                Locale.getDefault(), "Received response for %s in %.1fms%n%s",
                response.request().url(), (t2 - t1) / 1e6, response.headers()
            )
        )
        val mediaType = response.body()!!.contentType()
        val content = response.body()!!.string()
        Log.i(OKHTTP_LOG, "response body:$content")
        return response.newBuilder()
            .body(ResponseBody.create(mediaType, content))
            .build()
    }
}