package com.haierbiomedical.pricetag.retrofit

import android.util.Log
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.haierbiomedical.utils.SafeTypeAdapterFactory
import com.haierbiomedical.utils.SignUtils
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import okhttp3.Interceptor
import okhttp3.MediaType
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import okio.Buffer
import org.json.JSONObject
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


/**
 * Retrofit封装
 *
 * @author liyang
 */
const val OKHTTP_LOG = "OKHTTP_LOG"

class RetrofitUtils private constructor() {
    private var gson: Gson? = null

    init {
        gson = GsonBuilder().registerTypeAdapterFactory(SafeTypeAdapterFactory()).create()
    }

    private val apiUrl: ApiUrl
        get() =
            initRetrofit(initOkHttp()).create(ApiUrl::class.java)

    /**
     * 初始化Retrofit
     */
    private fun initRetrofit(client: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .client(client)
            .baseUrl(OKHTTP_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }

    /**
     * 初始化okhttp
     */
    private fun initOkHttp(): OkHttpClient {
        return OkHttpClient().newBuilder()
            .readTimeout(10, TimeUnit.SECONDS)
            .connectTimeout(10, TimeUnit.SECONDS)
            .writeTimeout(10, TimeUnit.SECONDS)
            .addInterceptor { chain: Interceptor.Chain ->
                try {
                    val request = chain.request()
                    val requestBuilder = request.newBuilder()
                    val oldRequestBody = request.body()
                    val requestBuffer = Buffer()
                    oldRequestBody?.writeTo(requestBuffer)
                    var oldBodyStr: String = requestBuffer.readUtf8()
                    requestBuffer.close()
                    val jsonParams = JSONObject()
                    jsonParams.put("appid", "hlmTag")
                    val sortMap = mapStringToMap(oldBodyStr)
                    oldBodyStr = Gson().toJson(sortMap)

                    val sign = SignUtils.generatorSign(sortMap, SignUtils.secret)
                    jsonParams.put("sign", sign)
                    val jsonData = JSONObject(oldBodyStr)
                    jsonParams.put("data", jsonData)

                    Log.d(OKHTTP_LOG, "${request.url()} json参数 = $jsonParams")
                    val requestBody =
                        RequestBody.create(
                            MediaType.parse("application/json"),
                            jsonParams.toString()
                        )
                    return@addInterceptor chain.proceed(requestBuilder.post(requestBody).build())
                } catch (e: Exception) {
                    throw e
                }
            }
            .addInterceptor(LogInterceptor())
            .retryOnConnectionFailure(true)
            .build()
    }

    private fun mapStringToMap(str: String): Map<String, String>? {
        val map = Gson().fromJson(str, Map::class.java) as Map<String, String>
        return map.toSortedMap()
    }

    companion object {
        @Volatile
        private var mApiUrl: ApiUrl? = null
        var OKHTTP_URL = "https://vimstest.haierbiomedical.com/api/"

        /**
         * 单例模式
         */
        val apiUrl: ApiUrl?
            get() {
                if (mApiUrl == null) {
                    synchronized(RetrofitUtils::class.java) {
                        if (mApiUrl == null) {
                            mApiUrl = RetrofitUtils().apiUrl
                        }
                    }
                }
                return mApiUrl
            }
    }
}