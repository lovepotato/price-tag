package com.haierbiomedical.pricetag.retrofit

import com.haierbiomedical.pricetag.base.BaseResponse
import com.haierbiomedical.pricetag.entity.PriceTagRep
import com.haierbiomedical.pricetag.entity.PriceTagVaccInfoRsp
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.POST

interface ApiUrl {

    /**
     * 价签信息查询
     *
     * @param map
     * @return
     */
    @POST("ExternalAPI/tag/getTagInfo")
    fun getTagBindInfo(@Body map: Map<String, String?>): Observable<BaseResponse<PriceTagRep>>

    /**
     * 疫苗信息查询
     *
     * @param map
     * @return
     */
    @POST("ExternalAPI/tag/getCodeInfo")
    fun getCodeInfo(@Body map: Map<String, String?>): Observable<BaseResponse<PriceTagVaccInfoRsp>>

    /**
     * 价签绑定
     *
     * @param map
     * @return
     */
    @POST("ExternalAPI/tag/tagBindOrUpdate")
    fun tagBindOrUpdate(@Body map: Map<String, String?>): Observable<BaseResponse<String>>

    /**
     * 价签解绑
     *
     * @param map
     * @return
     */
    @POST("ExternalAPI/tag/unBindTag")
    fun unbindTag(@Body map: Map<String, String?>): Observable<BaseResponse<String>>
}