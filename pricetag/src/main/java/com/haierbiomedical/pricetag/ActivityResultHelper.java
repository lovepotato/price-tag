package com.haierbiomedical.pricetag;

import android.content.Context;
import android.content.Intent;

import androidx.annotation.Keep;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;

/**
 * @author 23015341
 */
@Keep
public class ActivityResultHelper {

    private static final String TAG = "ActivityLauncher";
    private Context mContext;
    private RouterFragment mRouterFragment;
    private static IScan mIscan;
    private static ActivityResultHelper activityResultHelper;

    public IScan getmIscan() {
        return mIscan;
    }

    public static ActivityResultHelper init(FragmentActivity activity, IScan iScan) {
        activityResultHelper = new ActivityResultHelper(activity, iScan);
        return activityResultHelper;
    }

    public static ActivityResultHelper getInstance() {
        return activityResultHelper;
    }

    private ActivityResultHelper(FragmentActivity activity, IScan iScan) {
        mContext = activity;
        mIscan = iScan;
        mRouterFragment = getRouterFragment(activity);
    }

    private RouterFragment getRouterFragment(FragmentActivity activity) {
        RouterFragment routerFragment = findRouterFragment(activity);
        if (routerFragment == null) {
            routerFragment = RouterFragment.newInstance();
            FragmentManager fragmentManager = activity.getSupportFragmentManager();
            fragmentManager
                    .beginTransaction()
                    .add(routerFragment, TAG)
                    .commitAllowingStateLoss();
            fragmentManager.executePendingTransactions();
        }
        return routerFragment;
    }

    private RouterFragment findRouterFragment(FragmentActivity activity) {
        return (RouterFragment) activity.getSupportFragmentManager().findFragmentByTag(TAG);
    }

    private void startActivityForResult(Class<?> clazz, Callback callback) {
        Intent intent = new Intent(mContext, clazz);
        startActivityForResult(intent, callback);
    }

    public void startPriceTag(String hospitalCode) {
        Intent intent = new Intent(mContext, PriceTagBindActivity.class);
        intent.putExtra("hospitalCode", hospitalCode);
        startActivityForResult(intent, null);
    }

    private void startActivityForResult(Intent intent, Callback callback) {
        mRouterFragment.startActivityForResult(intent, callback);
    }

    public interface Callback {
        void onActivityResult(int resultCode, Intent data);
    }
}