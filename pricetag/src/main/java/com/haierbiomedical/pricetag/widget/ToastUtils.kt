package com.haierbiomedical.pricetag.widget

import android.annotation.SuppressLint
import android.content.Context
import android.os.Handler
import android.os.Looper
import android.text.TextUtils
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.haierbiomedical.pricetag.R

@SuppressLint("StaticFieldLeak")
object ToastUtils {
    var toast: Toast? = null
    private val handler = Handler(Looper.getMainLooper())
    private var tvTips: TextView? = null
    private var toastView: View? = null
    private var ivToastIcon: ImageView? = null

    @SuppressLint("InflateParams")
    @JvmStatic
    fun toast(context: Context?, text: String?, boolean: Boolean = false) {
        if (TextUtils.isEmpty(text)) {
            return
        }
        handler.post {
            if (toast == null) {
                toast = Toast(context)
                toastView = LayoutInflater.from(context)
                    .inflate(R.layout.price_tag_toast_common, null, false)
                tvTips = toastView?.findViewById(R.id.tvToastTips)
                ivToastIcon = toastView?.findViewById(R.id.ivToastIcon)
                toast?.view = toastView
                toast?.setGravity(Gravity.CENTER, 0, 0)
            }
            ivToastIcon?.setImageResource(if (boolean) R.drawable.ic_price_tag_toast_success else R.drawable.ic_price_tag_toast_error)
            tvTips?.text = text
            toast?.show()
        }
    }

    @JvmStatic
    fun toast(context: Context?, text: String?) {
        toast(context, text, false)
    }

    @JvmStatic
    fun toastSuccess(context: Context?, text: String?) {
        toast(context, text, true)
    }
}