package com.haierbiomedical.pricetag.dialog

import android.app.Dialog
import android.content.Context
import android.os.Build
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.TextView
import com.haierbiomedical.pricetag.R

class PriceTagCommonTipDialog : Dialog {
    //    显示信息
    private lateinit var tipMessageTv: TextView

    //    取消按钮点击
    private lateinit var tipCancelBtn: TextView

    //    确认按钮点击
    private lateinit var tipSureBtn: TextView

    constructor(context: Context) : super(context) {
        initView()
    }


    constructor(context: Context, themeStyle: Int) : super(context, themeStyle) {
        initView()
    }

    private fun initView() {
        setContentView(R.layout.dialog_common_tip)

        setCanceledOnTouchOutside(false)

        tipMessageTv = findViewById(R.id.commonTipMessageTv)

        tipCancelBtn = findViewById(R.id.commonCancelTv)

        tipSureBtn = findViewById(R.id.commonSureTv)
        setCancelable(false)
    }

    class Builder(val context: Context) {

        private var message: String = ""
        private var cancelText: String = "取消"
        private var confirmText: String = "确定"
        private var hideCancelBtn: Boolean = false

        private var confirmListener: OnConfirmListener? = null
        private var cancelListener: OnCancelListener? = null

        fun setCancelText(cancel: String): Builder {
            this.cancelText = cancel
            return this
        }

        fun hideCancelBtn(): Builder {
            this.hideCancelBtn = true
            return this
        }

        fun setConfirmText(confirm: String): Builder {
            this.confirmText = confirm
            return this
        }

        fun setOnConfirmListener(confirmListener: OnConfirmListener): Builder {
            this.confirmListener = confirmListener
            return this
        }

        fun setOnCancelListener(cancelListener: OnCancelListener): Builder {
            this.cancelListener = cancelListener
            return this
        }

        fun setHintMessage(hint: String): Builder {
            message = hint
            return this
        }

        fun create(): PriceTagCommonTipDialog {
            val dialog = PriceTagCommonTipDialog(context, R.style.custom_dialog)

            dialog.tipMessageTv.text = message

            dialog.tipCancelBtn.setOnClickListener {
                dialog.dismiss()
                this.cancelListener?.onClick()
            }

            dialog.tipSureBtn.setOnClickListener {
                dialog.dismiss()
                this.confirmListener?.onClick()
            }

            dialog.tipCancelBtn.text = cancelText
            dialog.tipSureBtn.text = confirmText
            dialog.tipCancelBtn.visibility = if (hideCancelBtn) View.GONE else View.VISIBLE
            return dialog
        }
    }

    interface OnCancelListener {
        fun onClick()
    }

    interface OnConfirmListener {
        fun onClick()
    }

    override fun show() {
//        focusNotAle(window);
        super.show()
//        hideNavigationBar(window)
//        clearFocusNotAle(window)
    }

    private fun hideNavigationBar(window: Window?) {
        window?.decorView?.systemUiVisibility = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
        window?.decorView?.setOnSystemUiVisibilityChangeListener {
            var uiOptions: Int = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or  //布局位于状态栏下方
                    View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or  //全屏
                    View.SYSTEM_UI_FLAG_FULLSCREEN or  //隐藏导航栏
                    View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            uiOptions = if (Build.VERSION.SDK_INT >= 19) {
                uiOptions or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
            } else {
                uiOptions or View.SYSTEM_UI_FLAG_LOW_PROFILE
            }
            window.decorView.systemUiVisibility = uiOptions
        }
    }

    private fun focusNotAle(window: Window?) {
        window?.setFlags(
            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
        )
    }

    private fun clearFocusNotAle(window: Window?) {
        window?.clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE)
    }
}