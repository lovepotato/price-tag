package com.haierbiomedical.pricetag

import android.content.Context
import androidx.annotation.Keep

@Keep
interface IScan {
    /**
     * 扫码初始化
     *
     * @param context
     */
    fun initScan(context: Context)

    /**
     * 开始扫描
     */
    fun startScan()

    /**
     * 设置扫码回调
     * @param listener listener
     */
    fun setScanListener(listener: IScanListener)

    /**
     * 结束扫描
     */
    fun stopScan()

    /**
     * 清空扫描
     */
    fun cleanScan()
}

@Keep
interface IScanListener {
    fun getScanContent(string: String)
}